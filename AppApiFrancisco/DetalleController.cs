﻿using System;
using CoreAnimation;
using CoreGraphics;
using UIKit;
using System.IO;
using Foundation;

namespace AppApiFrancisco
{
    public partial class DetalleController : UIViewController
    {
        public CAGradientLayer gradiente;

        public DetalleController() : base("DetalleController", null)
        {
        }

        public DetalleController(IntPtr handle) : base(handle)
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public double Balance { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Image { get; set; }
        public string Background { get; set; }

        public DetalleController(int id, string name, string address, string email, int age, double balance, string latitude, string longitude, string image, string background)
        {
            Id = id;
            Name = name;
            Address = address;
            Email = email;
            Age = age;
            Balance = balance;
            Latitude = latitude;
            Longitude = longitude;
            Image = image;
            Background = background;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            gradiente = new CAGradientLayer()
            {
                Frame = View.Bounds,
                NeedsDisplayOnBoundsChange = true,
                MasksToBounds = true
            };

            gradiente.Colors = new CGColor[]
            {
                UIColor.FromRGBA(red:0.19f, green:0.28f, blue:0.33f, alpha:1.00f).CGColor,
                UIColor.FromRGBA(red:0.15f, green:0.63f, blue:0.85f, alpha:1.00f).CGColor,
            };

            View.Layer.InsertSublayer(gradiente, 0);

            lblId.Text = Id.ToString();
            lblName.Text = Name;
            lblAddress.Text = Address;
            lblEmail.Text = Email;
            lblAge.Text = Age.ToString();
            lblBalance.Text = Balance.ToString();

            string carpeta = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string nombre = $"{Name}.jpg";
            string fondo = $"{Name}bg.jpg";
            string archivo = Path.Combine(carpeta, nombre);
            string archivo2 = Path.Combine(carpeta, fondo);

            imgProfile.Image = UIImage.FromFile(archivo);
            imgDetail.Image = UIImage.FromFile(archivo2);
            imgDetail.Alpha = 0.5f;

            CALayer RedondeoImagen = imgProfile.Layer;
            RedondeoImagen.CornerRadius = 40;
            RedondeoImagen.MasksToBounds = true;

            CALayer RedondeoImagen2 = imgDetail.Layer;
            RedondeoImagen2.CornerRadius = 40;
            RedondeoImagen2.MasksToBounds = true;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

