﻿using System;
using UIKit;
using AppApiFrancisco.Entities;
using System.Collections.Generic;
using Foundation;
using CoreAnimation;
using CoreGraphics;

namespace AppApiFrancisco
{
    public class OrigenTabla : UITableViewSource
    {
        public string name, address, email, image;
        public int age, id;
        public double balance;

        public CAGradientLayer gradiente;
        List<User> TableElements;
        UIViewController Controlador;

        public OrigenTabla(List<User> users, UIViewController controlador)
        {
            TableElements = users;
            Controlador = controlador;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var Celda = (CeldaController)tableView.DequeueReusableCell("IDCelda", indexPath);
            var assist = TableElements[indexPath.Row];
            Celda.BackgroundColor = UIColor.SystemPurpleColor;
            Celda.TextLabel.BackgroundColor = UIColor.White;
            gradiente = new CAGradientLayer()
            {
                Frame = Celda.Bounds,
                NeedsDisplayOnBoundsChange = true,
                MasksToBounds = true
            };

            gradiente.Colors = new CGColor[]
            {
                UIColor.FromRGBA(red:0.19f, green:0.28f, blue:0.33f, alpha:1.00f).CGColor,
                UIColor.FromRGBA(red:0.15f, green:0.63f, blue:0.85f, alpha:1.00f).CGColor,
            };

            Celda.Layer.InsertSublayer(gradiente, 0);

            Celda.ActualizarCelda(assist);
            return Celda;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableElements.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            id = TableElements[indexPath.Row].id;
            name = TableElements[indexPath.Row].name;
            address = TableElements[indexPath.Row].address;
            email = TableElements[indexPath.Row].email;
            age = TableElements[indexPath.Row].age;
            balance = TableElements[indexPath.Row].balance;
            image = TableElements[indexPath.Row].image;

            var detail = Controlador.Storyboard.InstantiateViewController("DetalleController") as DetalleController;
            detail.Id = id;
            detail.Name = name;
            detail.Address = address;
            detail.Email = email;
            detail.Age = age;
            detail.Balance = balance;
            detail.Image = image;
            detail.Latitude = TableElements[indexPath.Row].latitude;
            detail.Longitude = TableElements[indexPath.Row].longitude;

            Controlador.PresentViewControllerAsync(detail, true);
        }
    }
}
