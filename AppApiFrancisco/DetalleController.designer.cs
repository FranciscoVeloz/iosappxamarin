// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace AppApiFrancisco
{
	[Register ("DetalleController")]
	partial class DetalleController
	{
		[Outlet]
		UIKit.UIImageView imgDetail { get; set; }

		[Outlet]
		UIKit.UIImageView imgProfile { get; set; }

		[Outlet]
		UIKit.UILabel lblAddress { get; set; }

		[Outlet]
		UIKit.UILabel lblAge { get; set; }

		[Outlet]
		UIKit.UILabel lblBalance { get; set; }

		[Outlet]
		UIKit.UILabel lblEmail { get; set; }

		[Outlet]
		UIKit.UILabel lblId { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		MapKit.MKMapView Mapa { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgProfile != null) {
				imgProfile.Dispose ();
				imgProfile = null;
			}

			if (Mapa != null) {
				Mapa.Dispose ();
				Mapa = null;
			}

			if (imgDetail != null) {
				imgDetail.Dispose ();
				imgDetail = null;
			}

			if (lblAddress != null) {
				lblAddress.Dispose ();
				lblAddress = null;
			}

			if (lblAge != null) {
				lblAge.Dispose ();
				lblAge = null;
			}

			if (lblBalance != null) {
				lblBalance.Dispose ();
				lblBalance = null;
			}

			if (lblEmail != null) {
				lblEmail.Dispose ();
				lblEmail = null;
			}

			if (lblId != null) {
				lblId.Dispose ();
				lblId = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}
		}
	}
}
