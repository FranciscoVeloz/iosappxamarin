﻿using System;
using Google.Maps;
using CoreLocation;
using CoreGraphics;
using UIKit;
using AppApiFrancisco.Services;

namespace AppApiFrancisco
{
    public partial class HistoryController : UIViewController
    {
        MapView mapa;

        public HistoryController() : base("HistoryController", null)
        {
        }

        public HistoryController(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            var service = new HistoryServices();

            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            try
            {
                var camera = CameraPosition.FromCamera(21.360483, -101.94785, 15, 30, 40);
                mapa = MapView.FromCamera(CGRect.Empty, camera);
                mapa.MapType = MapViewType.Normal;
                View = mapa;

                var data = await service.List();
                if (!data.status)
                {
                    return;
                }

                var lista = data.data;

                foreach (var item in lista)
                {
                    var Marcador = new Marker()
                    {
                        Title = item.date,
                        AppearAnimation = MarkerAnimation.Pop,
                        Position = new CLLocationCoordinate2D(item.latitude, item.longitude),
                        Map = mapa,
                    };
                }
            }
            catch (Exception ex)
            {
                DismissModalViewController(true);
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
