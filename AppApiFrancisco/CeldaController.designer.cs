// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace AppApiFrancisco
{
	[Register ("CeldaController")]
	partial class CeldaController
	{
		[Outlet]
		UIKit.UIImageView bgImage { get; set; }

		[Outlet]
		UIKit.UIImageView Image { get; set; }

		[Outlet]
		UIKit.UILabel lblAge { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblAge != null) {
				lblAge.Dispose ();
				lblAge = null;
			}

			if (Image != null) {
				Image.Dispose ();
				Image = null;
			}

			if (bgImage != null) {
				bgImage.Dispose ();
				bgImage = null;
			}
		}
	}
}
