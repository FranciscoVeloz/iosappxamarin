﻿using System;
using Google.Maps;
using CoreLocation;
using CoreGraphics;
using UIKit;
using AppApiFrancisco.Services;
using System.Timers;
using Xamarin.Essentials;

namespace AppApiFrancisco
{
    public partial class MapController : UIViewController
    {
        MapView mapa;
        UIAlertController Alerta;

        public MapController() : base("MapController", null)
        {
        }

        public MapController(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            base.ViewDidLoad();

            try
            {
                var camera = CameraPosition.FromCamera(21.360483, -101.94785, 15, 30, 40);
                mapa = MapView.FromCamera(CGRect.Empty, camera);
                mapa.MapType = MapViewType.Normal;
                View = mapa;

                //MarkerTimer();
                MainThread.BeginInvokeOnMainThread(() =>
                {
                    MarkerTimer();
                });
            }
            catch (Exception ex)
            {
                DismissModalViewController(true);
            }
        }

        public void MarkerTimer()
        {
            var timer = new Timer(5000);
            timer.Elapsed += AddMarker;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        private async void AddMarker(Object s, ElapsedEventArgs elapsed)
        {
            try
            {
                var mapService = new MapApiServices();

                var data = await mapService.List();
                if (!data.status)
                {
                    return;
                }

                var Marcador = new Marker()
                {
                    Title = "GPS",
                    Position = new CLLocationCoordinate2D(data.data.latitude, data.data.longitude),
                    Map = mapa,
                };
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public void MessageBox(string titulo, string mensaje)
        {
            Alerta = UIAlertController.Create(titulo, mensaje, UIAlertControllerStyle.Alert);
            Alerta.AddAction(UIAlertAction.Create("Aceptar", UIAlertActionStyle.Default, null));
            PresentViewController(Alerta, true, null);
        }
    }
}
