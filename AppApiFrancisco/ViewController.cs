﻿using Foundation;
using System;
using UIKit;
using AppApiFrancisco.Services;

namespace AppApiFrancisco
{
    public partial class ViewController : UIViewController
    {
        UIAlertController Alerta;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            var services = new UserServices();

            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            try
            {
                var data = await services.List();
                if (!data.status)
                {
                    MessageBox("Error", data.message);
                    return;
                }

                Tabla.Source = null;
                Tabla.RowHeight = 279f;
                Tabla.BackgroundColor = UIColor.SystemPurpleColor;
                Tabla.Source = new OrigenTabla(data.data, this);
                Tabla.ReloadData();
            }
            catch (Exception ex)
            {
                MessageBox("Error", ex.Message);
            }
        }

        public void MessageBox(string titulo, string mensaje)
        {
            Alerta = UIAlertController.Create(titulo, mensaje, UIAlertControllerStyle.Alert);
            Alerta.AddAction(UIAlertAction.Create("Aceptar", UIAlertActionStyle.Default, null));
            PresentViewController(Alerta, true, null);
        }
    }
}
