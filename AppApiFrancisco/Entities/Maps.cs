﻿using System;
namespace AppApiFrancisco.Entities
{
    public class Maps
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }

    public class MapsResponse
    {
        public bool status { get; set; }
        public Maps data { get; set; }
        public string message { get; set; }
    }
}
