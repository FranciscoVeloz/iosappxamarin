﻿using System;
using System.Collections.Generic;

namespace AppApiFrancisco.Entities
{
    public class History
    {
        public int id { get; set; }
        public string description { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int fk_gps { get; set; }
        public string date { get; set; }
    }

    public class HistoryResponse
    {
        public bool status { get; set; }
        public List<History> data { get; set; }
        public string message { get; set; }
    }
}
