﻿using System.Collections.Generic;

namespace AppApiFrancisco.Entities
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int age { get; set; }
        public int balance { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string image { get; set; }
        public string background { get; set; }
    }

    public class UserResponse
    {
        public bool status { get; set; }
        public User data { get; set; }
        public string message { get; set; }
    }

    public class UsersResponse
    {
        public bool status { get; set; }
        public List<User> data { get; set; }
        public string message { get; set; }
    }
}
