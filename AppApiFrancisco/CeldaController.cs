﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CoreAnimation;
using Foundation;
using UIKit;
using AppApiFrancisco.Entities;

namespace AppApiFrancisco
{
    public partial class CeldaController : UITableViewCell
    {
        protected CeldaController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
        public async void ActualizarCelda(User user)
        {
            var rutaArchivo1 = await DescargarImagen(user.image, user.name);
            var rutaArchivo2 = await DescargarImagen(user.background, user.name + "bg");

            Image.Image = UIImage.FromFile(rutaArchivo1);
            bgImage.Image = UIImage.FromFile(rutaArchivo2);
            bgImage.Alpha = 0.5f;

            lblName.Text = user.name;
            lblName.TextColor = UIColor.White;

            lblAge.Text = user.age.ToString();
            lblAge.TextColor = UIColor.White;

            CALayer RedondeoImagen = Image.Layer;
            RedondeoImagen.CornerRadius = 40;
            RedondeoImagen.MasksToBounds = true;
        }

        public async Task<string> DescargarImagen(string ruta, string name)
        {
            WebClient client = new WebClient();
            byte[] imagenDatos = await client.DownloadDataTaskAsync(ruta);
            string carpeta = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string nombre = $"{name}.jpg";
            string archivo = Path.Combine(carpeta, nombre);
            File.WriteAllBytes(archivo, imagenDatos);
            return archivo;
        }
    }
}

