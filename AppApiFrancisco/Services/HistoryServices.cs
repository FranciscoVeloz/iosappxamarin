﻿using System;
using System.Text.Json;
using System.Net.Http;
using AppApiFrancisco.Entities;
using System.Threading.Tasks;

namespace AppApiFrancisco.Services
{
    public class HistoryServices
    {
        HttpClient client = new HttpClient();
        const string URL = "https://xamarin-app-gps.herokuapp.com/api/history/1";

        public async Task<HistoryResponse> List()
        {
            try
            {
                var response = await client.GetAsync(URL);
                var data = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<HistoryResponse>(data);

                return result;
            }
            catch (Exception ex)
            {
                return new HistoryResponse()
                {
                    status = false,
                    message = ex.Message
                };
            }
        }
    }
}
