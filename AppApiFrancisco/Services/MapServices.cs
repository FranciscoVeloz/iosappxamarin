﻿using System;
using System.Text.Json;
using System.Net.Http;
using AppApiFrancisco.Entities;
using System.Threading.Tasks;

namespace AppApiFrancisco.Services
{
    public class MapApiServices
    {
        HttpClient client = new HttpClient();
        const string URL = "https://xamarin-app-gps.herokuapp.com/api/maps/1";

        public async Task<MapsResponse> List()
        {
            try
            {
                var response = await client.GetAsync(URL);
                var data = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<MapsResponse>(data);

                return result;
            }
            catch (Exception ex)
            {
                return new MapsResponse()
                {
                    status = false,
                    message = ex.Message
                };
            }
        }
    }
}
