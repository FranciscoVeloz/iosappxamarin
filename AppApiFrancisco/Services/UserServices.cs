﻿using System;
using System.Text.Json;
using System.Net.Http;
using AppApiFrancisco.Entities;
using System.Threading.Tasks;

namespace AppApiFrancisco.Services
{
    public class UserServices
    {
        HttpClient client = new HttpClient();
        const string URL = "https://iosapifrank.azurewebsites.net/Principal/ConsultarSQLServer";

        public async Task<UsersResponse> List()
        {
            try
            {
                //throw new Exception("Error de prueba");
                var response = await client.GetAsync(URL);
                var data = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<UsersResponse>(data);

                return result;
            }
            catch (Exception ex)
            {
                return new UsersResponse()
                {
                    status = false,
                    message = ex.Message
                };
            }
        }
    }
}
